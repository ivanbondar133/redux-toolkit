import {UserAction, UserActionTypes, UserState} from "types/user";

const initialState: UserState = {
	page: 1,
	users: [],
	loading: false,
	error: null,
}

export const userReducer = (state = initialState, action: UserAction): UserState => {
	switch (action.type) {
		case UserActionTypes.FETCH_USERS:
			return {
				...state,
				loading: true,
				error: null,
				users: []
			}
		case UserActionTypes.FETCH_USERS_SUCCESS:
			return {
				...state,
				loading: false,
				error: null,
				users: action.payload.map(user => ({id: user.id, name: user.name}))
			}
		case UserActionTypes.FETCH_USERS_ERROR:
			return {
				...state,
				loading: false,
				error: action.payload,
				users: []
			}
		case UserActionTypes.ADD_USER:
			return {
				...state,
				users: [
					...state.users,
					action.payload,
				],
			}
		case UserActionTypes.EDIT_USER:
			const idx = state.users.findIndex(user => user.id === action.payload.id)
			return {
				...state,
				users: [
					...state.users.slice(0, idx),
					action.payload,
					...state.users.slice(idx + 1),
				],
			}
		case UserActionTypes.REMOVE_USER:
			return {
				...state,
				users: state.users.filter(u => action.payload.id !== u.id),
			}
		case UserActionTypes.SET_PAGE:
			return {
				...state,
				page: action.payload
			}
		default:
			return state
	}
}