import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import {rootReducer} from "./reducers";

export const setupStore = () => createStore(rootReducer, applyMiddleware(thunk))

export type AppStore = ReturnType<typeof setupStore>
export type AppDispatch = AppStore['dispatch']
