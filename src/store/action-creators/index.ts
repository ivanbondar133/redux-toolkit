import * as UserActionCreators from './user'
import * as TodoActionCreators from './todo'

const actionCreators = {
	...UserActionCreators,
	...TodoActionCreators,
}

export default actionCreators
