import {axios} from "hooks/useAxios";
import {User, UserAction, UserActionTypes} from "types/user";
import {Dispatch} from "redux";

export const fetchUsers = (page: number) => {
	return async (dispatch: Dispatch<UserAction>) => {
		try {
			dispatch({type: UserActionTypes.FETCH_USERS})
			const users = await axios.get('/users', {
				params: {
					_page: page,
					_limit: 5
				}
			})
			dispatch({type: UserActionTypes.FETCH_USERS_SUCCESS, payload: users.data})
		} catch (e) {
			dispatch({type: UserActionTypes.FETCH_USERS_ERROR, payload: (e as Error).message})
		}
	}
}

export const addUser = (userName: string) => {
	return async (dispatch: Dispatch<UserAction>) => {
		const user = {name: userName} as User
		const {data} = await axios.post<User>('/users', {...user})
		dispatch({type: UserActionTypes.ADD_USER, payload: data})
	}
}

export const editUser = (user: User) => {
	return async (dispatch: Dispatch<UserAction>) => {
		const {data} = await axios.put<User>(`/users/${user.id}`, {...user})
		dispatch({type: UserActionTypes.EDIT_USER, payload: data})
	}
}

export const removeUser = (id: number) => {
	return async (dispatch: Dispatch<UserAction>) => {
		await axios.delete(`/users/${id}`)
		dispatch({type: UserActionTypes.REMOVE_USER, payload: {id}})
	}
}

export const setPage = (page: number) => ({type: UserActionTypes.SET_PAGE, payload: page})
