import React, {useState} from "react";
import {Button} from "react-bootstrap";
import {useActions} from "hooks/useActions";
import Layout from "components/Layout";
import ItemModal from "../components/ItemModal";
import {Todo} from "../types/todo";
import TodoList from '../components/TotoList'

const Index = () => {
	const {addTodo} = useActions()

	const [showModal, setShowModal] = useState(false);

	const handleAdd = async (title: string) => {
		const newTodo = {
			title,
			completed: false,
		} as Todo

		await addTodo(newTodo)
		setShowModal(false)
	}
	return (
		<Layout>
			<h1 className="h1 mb-4">Todos</h1>
			<Button variant="primary" onClick={() => setShowModal(true)}>Add Todo</Button>
			<TodoList />
			<ItemModal
				show={showModal}
				setShow={setShowModal}
				title={'Add Todo'}
				handleSave={handleAdd}
			/>
		</Layout>
	)
}

export default Index
