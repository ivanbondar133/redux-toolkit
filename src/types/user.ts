export interface User {
	id: number
	name: string
}

export enum UserActionTypes {
	FETCH_USERS = 'FETCH_USERS',
	FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS',
	FETCH_USERS_ERROR = 'FETCH_USERS_ERROR',
	ADD_USER = 'ADD_USER',
	REMOVE_USER = 'REMOVE_USER',
	EDIT_USER = 'EDIT_USER',
	SET_PAGE = 'SET_PAGE',
}

export interface UserState {
	page: number
	users: User[]
	loading: boolean
	error: string | null
}

interface FetchUsersAction {
	type: UserActionTypes.FETCH_USERS
}

interface FetchUsersError {
	type: UserActionTypes.FETCH_USERS_ERROR
	payload: string
}

interface FetchUserSuccess {
	type: UserActionTypes.FETCH_USERS_SUCCESS
	payload: Array<User>
}

interface AddUser {
	type: UserActionTypes.ADD_USER
	payload: User
}

interface RemoveUser {
	type: UserActionTypes.REMOVE_USER
	payload: {id: number}
}

interface EditUser {
	type: UserActionTypes.EDIT_USER
	payload: User
}

interface SetPage {
	type: UserActionTypes.SET_PAGE
	payload: number
}

export type UserAction = FetchUsersAction | FetchUsersError | FetchUserSuccess | AddUser | RemoveUser | EditUser | SetPage