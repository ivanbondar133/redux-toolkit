import React, {useEffect, useState} from "react";
import {ListGroup, Pagination} from "react-bootstrap";
import TodoItem from "./TodoItem";
import {useTypedSelector} from "../hooks/useTypedSelector";
import {useActions} from "../hooks/useActions";

const TotoList = () => {
	const [page, setPage] = useState(1)

	const {fetchTodos} = useActions()

	const {todos, error, loading} = useTypedSelector(state => state.todo)

	useEffect(() => {
		fetchTodos(page)
	}, [page]);

	if (error) {
		return <div>{error}</div>
	}

	if (loading) {
		return <div>Loading...</div>
	}
	return (
		<>
			<ListGroup className={'mt-4'}>
				{
					todos.map(todo => {
						return (
							<TodoItem todo={todo} key={todo.id} />
						)
					})
				}

			</ListGroup>
			<Pagination className={'mt-3'}>{
				[...Array(2)].map((i, n) => {
					return (
						<Pagination.Item onClick={() => setPage(n + 1)} key={n} active={(n + 1) === page}>
							{n + 1}
						</Pagination.Item>
					)
				})
			}</Pagination>
		</>
	)
}

export default TotoList
