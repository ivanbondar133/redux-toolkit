# Reduxt toolkit demo app

To start work with this project do next steps:

1. clone repo
2. copy db.cache.json to db.json
3. run `yarn install`
4. run `yarn server` and `yarn start`
5. have fun

Repo contains 3 branches. `master` contains basic redux usage. 

In `redux-toolkit` branch entity `User` has been rewritten to use RTK.

In `redux-toolkit-query` branch in entity `Todo` you can find example of basic usage TRK-query utility.